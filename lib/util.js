
export function isInViewPort(ele) {
  const viewWidth = window.innerWidth || document.documentElement.clientWidth
  const viewHeight = window.innerHeight || document.documentElement.clientHeight
  const { top, bottom, left, right } = ele.getBoundingClientRect()
  let boolean = top >= 0 && left >= 0 && bottom <= viewHeight && right <= viewWidth
  return boolean
}

