import throttle from 'lodash.throttle'
import { isInViewPort } from './util'

var exposureArr = []   // 1.收集所有暴露过的数据
var upArr = []         // 2.收集将要上报数据 
var exposedArr = []    // 3.收集已上报的数据

function Exposure(time, type) {
  this.time = time || 10000
  this.type = type
}

Exposure.prototype.intExposure = function (request, that) {
  this.setTimeoutExposure(request)
  window.addEventListener('scroll', throttle(this.scrollExposure, 300))

  /* 当窗口即将被卸载（关闭）时,会触发该事件 */
  window.onbeforeunload = () => {
    this.requestExposure(request)
  }

  window.onunload = () => {
    this.requestExposure(request)
  }

  /* 点击浏览器前进后退时触发 */
  window.onpopstate = () => {
    this.requestExposure(request)
  }

  if (this.type === 'react') {
    that.props.history.listen(() => {
      this.requestExposure(request)
    })
  }
  //todo 监听vue 路由变化上报
}

Exposure.prototype.removeExposure = function () {
  clearTimeout(window.reportTimer)
  window.removeEventListener('scroll', this.scrollExposure)
}

Exposure.prototype.clearExposureArr = function () {
  exposureArr = []
}

Exposure.prototype.setTimeoutExposure = function (request) {
  const reportTick = () => {
    this.requestExposure(request)
    window.reportTimer = setTimeout(reportTick, this.time)
  }
  clearTimeout(window.reportTimer)
  window.reportTimer = setTimeout(reportTick, this.time)
}

Exposure.prototype.scrollExposure = function () {
  if (this.type === 'vue') {
    if (!window.__this__.$refs || !window.__this__.$refs.ulRef) return
    const arr = window.__this__.$refs.ulRef.$children
    arr.forEach(ele => {
      const dataId = ele.$attrs['data-id']
      if (exposureArr && exposureArr.indexOf(dataId) == -1 && isInViewPort(ele.$el) && dataId != null) {
        exposureArr.push(dataId)
      }
    })
  } else if (this.type === 'react') {
    if (!window.__this__.refs || !window.__this__.refs.ulRef) return
    const arr = window.__this__.refs.ulRef.childNodes
    arr.forEach(ele => {
      const dataId = ele.getAttribute('data-id')
      if (exposureArr && exposureArr.indexOf(dataId) == -1 && isInViewPort(ele) && dataId != null) {
        exposureArr.push(dataId)
      }
    })
  }
}

Exposure.prototype.requestExposure = function (request) {
  upArr = exposureArr.filter(el => !exposedArr.includes(el))
  if (upArr.length == 0) return
  request(upArr)
  exposedArr = exposedArr.concat(upArr)
  upArr = []
}

export default Exposure

